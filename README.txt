CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Support requests
 * Maintainers


INTRODUCTION
------------

Confirm Leave module warns users when they're about to navigate 
away from an unsaved node edit page. 

For a full description of the module, visit the project page:
https://www.drupal.org/project/confirm_leave


INSTALLATION
------------

 * Run composer to install the dependencies.
   composer require 'drupal/confirm_leave:^1.0'

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/docs/8/extending-drupal-8
   /installing-drupal-8-modules for further information.

 * A more comprehensive installation instruction for 
   Drupal 8 can be found at: https://www.drupal.org/node/2923804


SUPPORT REQUESTS
----------------

Before posting a support request, carefully read the installation
instructions provided in module documentation page.

Before posting a support request, check Recent log entries at
https://site-url/admin/reports/dblog

Once you have done this, you can post a support request at module issue queue:
https://www.drupal.org/project/issues/confirm_leave

When posting a support request, please inform if you were able to see any errors
in Recent log entries.


MAINTAINERS
-----------

Current maintainers:
 * mpriscella - https://www.drupal.org/u/mpriscella
