/**
 * @file
 * confirm-leave.js
 */

(function ($, Drupal) {

  'use strict';

  /**
   * If any input in the form is modified, add a confirm message on unload.
   *
   * @type {Object}
   */
  Drupal.behaviors.confirmLeave = {
    attach: function (context, settings) {
      $('.form-item').on('formUpdated', function (e) {
        $('form').addClass('form-updated');

        window.onbeforeunload = function () {
          return Drupal.t('Are you sure?');
        }

        $('form').on('submit', function (e) {
          window.onbeforeunload = null;
        });
      });
    }
  };

}(jQuery, Drupal));
